import time

from app import celery


@celery.task()
def low_priority(x):
    limited_f(x)
    return


@celery.task()
def high_priority(x):
    limited_f(x)
    return


def limited_f(x):
    time.sleep(10)
    return True

