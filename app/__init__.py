from flask import Flask
from app.mcelery import make_celery

app = Flask(__name__)
app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379'
)
celery = make_celery(app)
celery.conf.task_routes = ([
    ('app.tasks.low_priority', {'queue': 'lowpriority'}),
    ('app.tasks.high_priority', {'queue': 'highpriority'}),

],)

task_lists = []

from app import views
from app import tasks


