from app import app, task_lists
from app.task import MyTask

from app.tasks import limited_f, high_priority, low_priority
from celery.result import AsyncResult
from flask import render_template, request, jsonify


@app.route("/add", methods=["GET", "POST"])
def intermediary():
    weight = int(request.args.get("weight"))
    user_id = request.args.get("user-id")

    # Here you can implement logic for load balancing
    if weight > 1:
        task = high_priority.delay("parameter")
    else:
        task = low_priority.delay("Parameter")


    # task = limited_f.delay("parameter")
    t = MyTask(task.id, weight)
    task_lists.append(t)

    return "Done"


@app.route("/test", methods=["GET", "POST"])
def test():
    weight = request.args.get("weight")
    user_id = request.args.get("user-id")
    return weight + user_id


@app.route("/result", methods=["GET", "POST"])
def show_result():
    result = "{"
    for task in task_lists:
        print(f"task id = {task.user_id} and status = {AsyncResult(task.user_id).result}")
        result += "{"
        result += f"user-id : {task.user_id} , status : {AsyncResult(task.user_id).status}"
        result += "},"

    result = result[:len(result) - 1]
    result += "}"
    return result
