# pull official base image
FROM python:3.9.1-slim-buster

RUN mkdir /opt/first_app
# set work directory
WORKDIR /opt/first_app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV FLASK_APP=run.py

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# copy project
COPY . .

EXPOSE 5000

CMD ["flask","run","--host","0.0.0.0"]